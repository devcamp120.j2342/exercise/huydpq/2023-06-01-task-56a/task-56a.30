package com.devcamp.employees.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employees.models.Employees;

@CrossOrigin
@RequestMapping
@RestController
public class EmployeesController {
    @GetMapping("/employees")
    public ArrayList<Employees> getAllEmployees() {

        ArrayList<Employees> employees = new ArrayList<>();


        Employees emp1 = new Employees(1, "John", "Doe", 5000);
        Employees emp2 = new Employees(2, "Mary", "Smith", 6000);
        Employees emp3 = new Employees(3, "Tom", "Hanks", 7000);
        
        System.out.println(emp1);
        System.out.println(emp2);
        System.out.println(emp3);
        
        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);

        return employees;
    }
}
